/* 
 *	Jeu de devinette - créé par Natan FOURIÉ
 */

// Cette ligne génère aléatoirement un nombre entre 1 et 100.
var solution = Math.floor(Math.random() * 100) + 1;

/*
 *	DEBUG
 *	// console.log("(La solution est " + solution + ")");
 */

var proposition = -1; // Valeur initiale forcément fausse.
var numeroEssai = 0;

alert("Bienvenue dans ce jeu de devinette !");

// Le jeu continue tant que la proposition est incorrecte et que le nombre maximal d'essais (6) n'est pas atteint.
while ((proposition !== solution) && (numeroEssai < 6)) {
    numeroEssai++;
    proposition = Number(prompt("Entrez votre proposition " + numeroEssai + " :"));
    if (proposition < solution) {
		alert(proposition + " est trop petit. \nIl vous reste " + (6 - numeroEssai) + " essais.");
    } else if (proposition > solution) {
		alert(proposition + " est trop grand.");
    }
}
// Ici, soit le joueur a trouvé la solution, soit le nombre d'essais est dépassé.
if (proposition === solution) {
    alert("Bravo ! La solution était " + solution + ". \nVous avez trouvé en " + numeroEssai + " essai(s).");
} else {
    alert("Perdu, il ne vous reste plus aucun essais... \nLa solution était: " + solution + ". \nRetentez votre chance !");
	var txt;
	if (confirm("Voulez vous rejouer?")) {
		window.location.href = "devinette.html";
	} else {
		txt = "Merci pour votre participation et à bientôt! :)";
	}
	document.getElementById("container").innerHTML = txt;
}