// Pure JS:
document.addEventListener('DOMContentLoaded', function() {
	document.getElementById("load-this").addEventListener("load", saluer);
	document.getElementById("click-this").addEventListener("click", saluer);
});

// The handler also must go in a .js file
function handler() {
  /* ... */
}