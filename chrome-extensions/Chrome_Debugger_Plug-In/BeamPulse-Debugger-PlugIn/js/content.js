console.log("content.js fonctionne");

// console.log(Object.keys(localStorage).length);

chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    console.log(request.statut);

    switch (request.statut) {

    	case "start":
    		sendResponse(localStorage);
    		console.log(sender);
    		break;

    	case "sync":
    		localStorage.setItem(request.key, request.value);
    		sendResponse(localStorage);
    		console.log("synchronisation");
    		break;

    	case "stop":
    		console.log("stop");
    		break;

    	default:
    		console.log("Valeur request.statut non reconnue");

    }   

});