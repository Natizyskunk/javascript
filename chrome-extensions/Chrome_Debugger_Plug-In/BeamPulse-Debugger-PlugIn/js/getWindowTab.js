function lancerFonction(callback,fenetre,tab) // son rôle est de lancer la fonction callback avec tab en paramètre
		{
		if (!fenetre)// premier appel : 1 seul paramètre
			{
			chrome.windows.getLastFocused(function(fenetre){lancerFonction(callback,fenetre);}); /* on récupère la fenêtre en prenant soin de conserver callback */
			}
		else
			{
			if (!tab) // deuxième appel : 2 paramètres
				{
				chrome.tabs.getSelected(fenetre.id,function(tab){lancerFonction(callback,fenetre,tab);});
				}
			else // troisième appel : tous les paramètres
				{
				callback(tab);
				}
			}
		}
	
	function newTab(tab)
		{
		chrome.tabs.create({url:tab.url});
		}