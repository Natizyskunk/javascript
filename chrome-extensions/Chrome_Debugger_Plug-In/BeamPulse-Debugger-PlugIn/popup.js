window.onload = function() {	

	console.log("popup.js fonctionne");
	
	function replaceValue(event) {
		//let currentValue = this.textContent;
		let parentElement = this.parentNode;
		let parentRow = parentElement.parentNode;
		let value = prompt("Veuillez entre une nouvelle valeur");
		let elementKey = parentRow.childNodes[1].textContent;
		//console.log(elementKey);
		sync(elementKey, value);		
	}

	function sync(key, value) {
		chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
			chrome.tabs.sendMessage(tabs[0].id, {statut: "sync", key: key, value: value}, function(response) {
				buildTable(response);
				console.log(response);
			});
		});
	}

	var boutonElt = document.getElementById("bouton");
	boutonElt.addEventListener('click', function() {
		
		chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
		  	chrome.tabs.sendMessage(tabs[0].id, {statut: "start"}, function(response) {
		  		buildTable(response);
		  	});
		});
	})

	let syncElt = document.getElementById("btnSync");
	syncElt.addEventListener('click', function() {
		chrome.tabs.reload();
		// chrome.tabs.getSelected(null, function(tab) {
		// 	console.log(tab.id);
		// });
	});

	window.buildTable = function(obj) {

		let variablesElt = document.getElementById("variables");
		variablesElt.innerHTML = "";

		var tableElt = document.createElement("table");
		tableElt.classList.add("table");
		var theadElt = document.createElement("thead");
		var trElt = document.createElement("tr");
		var th1Elt = document.createElement("th");
		th1Elt.textContent = "Nom";
		var th2Elt = document.createElement("th");
		th2Elt.textContent = "Valeur";
		var th3Elt = document.createElement("th");
		th3Elt.textContent = "Action";
		var th4Elt = document.createElement("th");
		th4Elt.textContent = "---";

		trElt.append(th1Elt, th2Elt, th3Elt, th4Elt);
		theadElt.appendChild(trElt);
		tableElt.appendChild(theadElt);

		variablesElt.appendChild(tableElt);
		
		if (obj) {

			let i = 1;
			var tbodyElt = document.createElement("tbody");

			for (key in obj) {
				var rowElt = document.createElement("tr");
				rowElt.setAttribute("id", "row" + i);
				var headElt = document.createElement("th");
				headElt.textContent = i;

				var data1Elt = document.createElement("td");
				data1Elt.textContent = key;
				data1Elt.classList.add("variablenom" + i);

				var data2Elt = document.createElement("td");
				data2Elt.textContent = obj[key];
				//data2Elt.addEventListener('click', replaceValue);

				var data3Elt = document.createElement("td");
				var bouton3Elt = document.createElement("input");
				bouton3Elt.type = "button";
				bouton3Elt.value = "modifier";
				bouton3Elt.classList.add("btn", "btn-primary", "btn-sm", "modification");
				bouton3Elt.addEventListener('click', replaceValue);

				data3Elt.appendChild(bouton3Elt);
				rowElt.append(headElt, data1Elt, data2Elt, data3Elt);
				tbodyElt.appendChild(rowElt);
				tableElt.appendChild(tbodyElt);

				i++;
				
				
				
			}
			
		}

	}

}

