// Pure JS:
document.addEventListener('DOMContentLoaded', function() {
	document.getElementById("save").addEventListener("click", enregistrer);
	document.getElementById("new-tab").addEventListener("click", newTab);
	document.getElementById("new-window").addEventListener("click", newWindow);
});

// The handler also must go in a .js file
function handler() {
  /* ... */
}